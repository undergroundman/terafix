#! /bin/bash

[ ! -s ~/.config/terafix ] && echo -e 'backup=true\nlocalipbase=192.168.1.0\nprecleanup=false\nstatus=none' > ~/.config/terafix # if config file not found so we create it.

configur() # this iexample usage: configur load backup or configur save backup true
{
case $1 in 
save)
grep -v "^$2=" ~/.config/terafix > /tmp/terafix.tmp
echo $2=$3  >> /tmp/terafix.tmp
mv /tmp/terafix.tmp ~/.config/terafix
;;
load)
grep "^$2=" ~/.config/terafix | cut -f2 -d '=' 
;;
esac
}
export -f configur
enable()
{
localipbase=$(configur load localipbase)
precleanup=$(configur load precleanup)
[ $precleanup == "true" ] && gksu "iptables -F ;  iptables -X" 
gksu "iptables -N terafix
 iptables -I INPUT -s ${localipbase}/24 -j terafix
 iptables -I INPUT ! -s ${localipbase}/24 -j terafix
iptables -I OUTPUT -d ${localipbase}/24 -j terafix
iptables -I OUTPUT ! -d ${localipbase}/24 -j terafix
iptables -A terafix -s ${localipbase}/24
iptables -A terafix ! -s ${localipbase}/24
iptables -A terafix -d ${localipbase}/24
iptables -A terafix ! -d ${localipbase}/24
systemctl enable iptables
systemctl start iptables"
configur save status enable
}
export -f enable
disable()
{
localipbase=$(configur load localipbase)
gksu "
iptables -D INPUT -s ${localipbase}/24 -j terafix
iptables -D INPUT ! -s ${localipbase}/24 -j terafix
iptables -D OUTPUT -d ${localipbase}/24 -j terafix
iptables -D OUTPUT ! -d ${localipbase}/24 -j terafix
iptables -F terafix
iptables -X terafix
iptables-save > /etc/iptables/iptables.rules"
$(Xdialog  --cancel-label "خیر" --ok-label "بله" --title "سرویس فایروال" --yesno "آیا می خواهید سرویس فایروال غیر فعال شود؟" 0 0) && gksu "systemctl stop iptables ; systemctl disable iptables"
configur save status disable
}
export -f disable
log()
{
date
echo -e "\nوضعیت: $(configur load status)"
 [ $(configur load status) == "disable" ] && return

loglist=(`gksu "iptables -L terafix -nvx | sed -e 's/ * / /g' -e '1,2d'| cut -f3 -d ' ' " 2>&1 | tail -4 `)
echo "
دانلود از اینترنت: ${loglist[1]} بایت

آپلود به اینترنت: ${loglist[3]} بایت

دانلود از شبکه محلی: ${loglist[0]} بایت

آپلود به شبکه محلی: ${loglist[2]} بایت"
}
export -f log
save_log ()
{
log_file=$(Xdialog --stdout --fselect ~/ 0 0)
log >> $log_file
}
export -f save_log
reset_counter()
{
gksu "iptables -Z terafix"
}
export -f reset_counter
export USR_HELP='
<window title="مستندات">
  <vbox scrollable="true" width="600" height="300">
  <hbox>
   <text selectable="true" use-markup="true"><label>"<span underline='"'single'"' color='"'blue'"'>https://gitlab.com/undergroundman/terafix</span>"</label><action signal="button-press-event">xdg-open https://gitlab.com/undergroundman/terafix &</action></text>
  <text><label>آدرس برنامه:</label></text>
 </hbox>
    <text selectable="false" wrap="true" width-chars="60" use-markup="true">
    <label>"                        <b>راهنمای کاربر</b>
    <b>درباره برنامه</b>
    ---------------------------
          این برنامه برای مانیتورینگ مصرف اینترنت نوشته شده است. و با iptables کار می کند.
          اگر این برنامه را مفید دانستید به نوسعه کدها یا مستندات برنامه کمک کنید.
              آدرس اینترنتی برنامه: https://gitlab.com/undergroundman/terafix

          
          <b>تقسیم بندی فضاهای صفحه اصلی</b>
          --------------------------------------------------------
          صفحه اصلی سه قسمت اصلی دارد:
          ۱) قسمت Action که در برگیرنده ی گزینه های مربوط به اعمال اصلی برنامه است.
          ۲)قسمت Log که مخصوص گزارش ترافیک است.
          ۳)قسمت Interface که گزینه های معمول یک برنامه در آن قسمت قرار دارد.
          
          
          <b>گزینه های صفحه ی اصلی</b>
          -----------------------------------------------
          <i>فعال کردن</i>: این گزینه نرم افزار iptables را تنظیم می کند تا ترافیک ورودی و خروجی را محاسبه کند.و سرویس های لازم را نیز اجرا می کند.
          
          <i>غیرفعال کردن</i>: این گزینه تنظیماتی که توسط همین برنامه اعمال شده را پاک می کند و در صورت تمایل نرم افزار iptables را از لیست اجرای خودکار بیرون می آورد.و همچنین تمامی گزارش هایی که ذخیره نشده اند را نیز از بین می برد.
          
          <i>رست شمارنده</i>: این گزینه شمارنده ها را به نقطه صفر برمی گردانند. مورد استفاده آن برای زمانی است که مثلا اینترنت ماهانه دارید و یک ماه جدید را می خواهید شروع کنید.
          
          <i>توقف</i>: این گزینه موقتا شمارنده را متوقف می کند
          
          <i>شروع</i>: این گزینه برای از سرگیزی محاسبه حجم استفاده می شود این گزینه زمانی کاربر دارد که شما  قبلا شمارنده را متوقف کرده باشید. 
        این گزینه برای فعال کردن برنامه نیست برای این منظور از گزینه <i>فعال کردن</i> استفاده کنید.
        
        <i>تنظیمات</i>: صفحه تنظیمات را باز می کند.
        
        <i>تازه سازی</i>: گزارش ترافیگ را چدید می کند.
        
        <i>ذخیره گزارش</i>: گزارش را ذخیره می کند.
        
        <b> صفحه تنظیمات</b>
        ----------------------------------
        <i>تهیه نسخه پشتیبان</i>: این گزینه فقط قبل از فعال شدن برنامه یک نسخه ی پشتیبان از فایل etc/iptables/iptables.rules تهیه می کند . اگر که از فایروال استفاده مخصوصی می کنید توصیه می شود که حتما این گزینه را تیک بزنید ولی اگر که تنظیماتی به فایروال اضافه نکردید این گزینه ضروری نیست. نکته ی مهم این است که آدرس این فایل در آرچ معتبر است و ممکن است که سایر توزیع ها از مسیر دیگری برای ذخیره استفاده کنند.
        این گزینه به ظور پیشفرض فعال است.
        
        <i>پاکسازی ruleهاو chain ها</i>: این گزینه قبل از فعال شدن برنامه و اضافه کردن شمارنده ها همه ی chainها و rule ها رو پاک می کند از این گزینه زمانی استفاده کنید که فایروال تنظیمات مخصوصی نداشته باشد و برنامه به خوبی کار نکند. در اینصورت با پاک کردن همه چیز احتمال دارد مشکل حل شود. . 
        این گزینه به خاطر مسائل امنیتی و جلوگیری از خرابکاری به طور پیشفرض غیرفعال است.
        
        <i>آی پی مبنا شبکه محلی</i>: در این برنامه دو نوع ترافیک وجود دارد ۱)ترافیک شبکه محلی ۲)ترافیک اینترنت. که هر آی پی که متعلق به این رنج باشد به عنوان شبکه محلی تعبیر می شود و هر آی پی خارج از این رنج به عنوان ترافیک اینترنت حساب می شود.
        رنج آی پی ها از a.b.c.0 تا a.b.c.254 می باشد مثلا 192.168.1.0 تا 192.168.1.254
        مقدار پیشفرض 192.168.1.0 می باشد.
        
    "</label>
  </text>
    <button>
      <label>Close</label>
      <variable>USR_HELP</variable>
      <action type="closewindow">USR_HELP</action>
    </button>
  </vbox>
  </window>
'
export DEV_HELP='
<window title=" مستندات توسعه دهنده">
  <vbox scrollable="true" width="600" height="300">
    <text  wrap="true" width-chars="60" use-markup="true">
    <label>"<b>راهنمای توسعه دهنده</b>
    <b>مکانیسم عمل</b>
    <b>سبک برنامه نویسی</b>
    <b>اطلاعات تکمیلی-جداول</b>
    <b>کارهایی که باید انجام داد</b>
    <b>توضیح</b>
    ---------------------------

    "</label>
  </text>
    <button>
      <label>Close</label>
      <variable>DEV_HELP</variable>
      <action type="closewindow">DEV_HELP</action>
    </button>
  </vbox>
  </window>
'
export SETTINGS="
<window title=\"تنظیمات\">
<vbox>
<checkbox>
    <default>$(configur load backup)</default>
      <label>تهیه نسخه پشتیبان از فایل /etc/iptables/iptables.rules </label>
      <variable>BACKUP</variable>
    </checkbox>
    <checkbox>
    <default>$(configur load precleanup)</default>
      <label>پاکسازی همه ی ruleها و chain ها(توصیه نمی شود)</label>
      <variable>PRECLEANUP</variable>
    </checkbox>
    <hbox><text><label>آی پی مبنا شبکه محلی</label></text><entry><default>\"$(configur load localipbase)\"</default><variable>LOCALIPBASE</variable></entry><text><label>\"/24\"</label></text></hbox>
    <hbox>
    <button>
    <label>ذخیره و خروج</label>
    <action>configur save backup \$BACKUP </action>
    <action>configur save localipbase \$LOCALIPBASE</action>
    <action>configur save precleanup \$PRECLEANUP</action>
    <action type=\"closewindow\">SETTINGS</action>
    </button>
    <button><variable>SETTINGS</variable><label>خروج بدون ذخیره</label><action type=\"closewindow\">SETTINGS</action></button>
    </hbox>
</vbox>
</window>
"
export MAIN_DIALOG='
<window title="Terafix v0.1">
 <hbox>
   <frame Action>
<button tooltip-text="فعال کردن برنامه"><label>فعال کردن</label><action>enable</action><variable>ENABLE</variable></button>
<button tooltip-text="غیرفعال کردن برنامه"><label>غیرفعال کردن</label><action>disable</action></button>
<button tooltip-text="صفرکردن همه حجم ها"><label>رست شمارنده</label><action>reset_counter</action></button>
<button tooltip-text="توقف موقتی برنامه"><label>توقف</label><variable>PAUSE</variable><action type="disable">PAUSE</action><action type="enable">START</action></button>
<button tooltip-text="شروع دوباره برنامه پس از یک توقف موقتی"><label>شروع</label><variable>START</variable><action type="disable">START</action><action type="enable">PAUSE</action></button>
<button tooltip-text="تنظیمات"><label>تنظیمات</label><action type="launch">SETTINGS</action></button>
   </frame>
   <frame LOG>
     <text>
       <input>log</input>
       <variable>LOG</variable>
     </text>
     <hbox>
       <button>
         <label>تازه سازی</label>
        <input file icon="reload"></input>
         <action type="refresh">LOG</action>
       </button>
      <button>
         <label>ذخبره گزارش</label>
        <input file icon="filesave"></input>
        <action>save_log</action>
       </button>
     </hbox>
   </frame>
   <vbox>
<frame Interface>
     <button tooltip-text="بستن رابط کاربری برنامه"><label>خروج</label><input file stock="gtk-close"></input></button>
     <button><label>راهنمای کاربر</label><action type="launch">USR_HELP</action></button>
     <button><label>راهنمای توسعه دهنده</label><action type="launch">DEV_HELP</action></button>
</frame>
   </vbox>
 </hbox>
 </window>
'

gtkdialog --program=MAIN_DIALOG
